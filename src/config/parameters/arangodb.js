const {
    ARANGO_PORT,
    ARANGO_USERNAME,
    ARANGO_ROOT_PASSWORD,
    ARANGO_HOST,
    ARANGO_DATABASE,
} = process.env;

export default {
    "host": ARANGO_HOST,
    "port": ARANGO_PORT,
    "username": ARANGO_USERNAME,
    "password": ARANGO_ROOT_PASSWORD,
    "database": ARANGO_DATABASE,
};