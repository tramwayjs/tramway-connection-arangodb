import { ArangoProvider } from '../../providers';

export default {
    "provider:arangodb": {
        "class": ArangoProvider,
        "constructor": [
            {"type": "parameter", "key": "arangodb"}
        ],
        "functions": []
    },
};