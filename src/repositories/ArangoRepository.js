import {Repository} from 'tramway-core-connection';

export default class ArangoRepository extends Repository {
    /**
     * 
     * @param {ArangoProvider} provider 
     * @param {Factory} factory 
     * @param {string} collectionName 
     */
    constructor(provider, factory, collectionName) {
        super(provider, factory, collectionName);
    }

    /**
     * @param {Object[]} items
     * 
     * @memberOf Repository
     */
    async createMany(items) {
        return await this.provider.createMany(items, this.collection);
    }

    /**
     * @param {Object[]} items
     * 
     * @memberOf Repository
     */
    async updateMany(items) {
        return await this.provider.createMany(items, this.collection);
    }

    /**
     * @param {string | Object} conditions
     * 
     * @memberOf Repository
     */
    async count(conditions) {
        const results = await this.provider.count(conditions, this.collection);
        const {count} = results;
        return count;
    }
}