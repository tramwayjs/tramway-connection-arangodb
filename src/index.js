import {
    ArangoProvider,
} from './providers';
import * as repositories from './repositories';
import * as config from './config';

export default ArangoProvider;
export {
    repositories,
    config,
};