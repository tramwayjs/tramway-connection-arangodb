import {Provider} from 'tramway-core-connection';
import {Database} from 'arangojs';

export default class ArangoProvider extends Provider {
    static COLLECTION_TYPE_DOCUMENT = 'document';
    static COLLECTION_TYPE_EDGE = 'edge';
    static COLLECTION_TYPE_GRAPH_EDGE = 'graph-edge';
    static COLLECTION_TYPE_GRAPH_VERTEX = 'graph-vertex';

    static INDEX_TYPE_GENERAL = 'general';
    static INDEX_TYPE_HASH = 'hash';
    static INDEX_TYPE_SKIPLIST = 'skiplist';
    static INDEX_TYPE_GEO = 'geo';
    static INDEX_TYPE_TEXT = 'fulltext';
    static INDEX_TYPE_PERSISTENT = 'persistent';
    static INDEX_TYPE_TTL = 'ttl';

    static DUPLICATE_ITEM_CODE = 409;
    static NOT_FOUND_CODE = 404;

    collectionTypes = new Map();
    collectionGraph = new Map();

    constructor(params = {}){
        super();
        this.database = params.database;
        this.setup(params);
    }

    resolveCollection(type, collectionName) {
        switch(type) {
            case ArangoProvider.COLLECTION_TYPE_GRAPH_VERTEX:
                return this.connection.graph(this.getCollectionGraph(collectionName)).vertexCollection(collectionName);
            case ArangoProvider.COLLECTION_TYPE_GRAPH_EDGE:
                return this.connection.graph(this.getCollectionGraph(collectionName)).edgeCollection(collectionName);
            case ArangoProvider.COLLECTION_TYPE_EDGE: 
                return this.connection.edgeCollection(collectionName);
            case ArangoProvider.COLLECTION_TYPE_DOCUMENT:
            default: 
                return this.connection.collection(collectionName);
        }
    }

    registerCollectionType(collectionName, type) {
        this.collectionTypes.set(collectionName, type);
        return this;
    }

    getCollectionType(collectionName) {
        return this.collectionTypes.get(collectionName);
    }

    registerCollectionGraph(collectionName, graphName) {
        this.collectionGraph.set(collectionName, graphName);
        return this;
    }

    getCollectionGraph(collectionName) {
        return this.collectionGraph.get(collectionName);
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * @returns {Promise<Object>}
     * 
     * @memberOf Provider
     */
    async getOne(id, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        let item;
        
        try {
            item = await collection.document(id);
        } catch(e) {
            if (ArangoProvider.NOT_FOUND_CODE !== e.statusCode) {
                throw e;
            }

            return;
        }

        return item;
    }

    /**
     * @param {string[] | number[]} ids
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async getMany(ids, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.lookupByKeys(ids);
    }

    /**
     * @param {string} collectionName
     * @returns {Object[]}
     * @memberOf Provider
     */
    async get(collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        const cursor = await collection.all();
        return await cursor.all();
    }

    /**
     * @param {string | Object} conditions
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async find(conditions, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        const cursor = await collection.byExample(conditions);
        return await cursor.all();
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * @returns {Promise<boolean>}
     * 
     * @memberOf Provider
     */
    async has(id, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.exists(id);
    }

    /**
     * @param { string[] | number[] } ids
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async hasThese(ids, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.exists(ids);
    }

    /**
     * @param {string | Object} conditions
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async count(conditions, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        let results;

        if (conditions) {
            results = await collection.byExample(conditions)
        } else {
            results = await collection.count();
        }

        const {count} = results || {};
        return count;
    }

    /**
     * @param {Entity} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async create(item, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        let result = await collection.save(item);
        item.setId(result._key);
        return item;
    }

    /**
     * @param {Object[]} items
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async createMany(items, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.import(items);
    }

    /**
     * @param {number|string} id
     * @param {Object} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async update(id, item, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        await collection.update(id, item);
        return item;
    }

    /**
     * @param {number|string} id
     * @param {Object} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async replace(id, item, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        await collection.replace(id, item);
        return item;
    }

    /**
     * @param {number|string} id
     * @param {Object} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async updateMany(items, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.bulkUpdate(items);
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async delete(id, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.remove(id);
    }

    /**
     * @param { number[] | string[]} id
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async deleteMany(ids, collectionName) {
        const type = this.getCollectionType(collectionName);
        const collection = this.resolveCollection(type, collectionName);
        return await collection.removeByKeys(ids);
    }

    /**
     * Recommended to use other functions first.
     * @param {string} query
     * @param {[] | Object} values
     * 
     * @memberOf Provider
     */
    async query(query, values) {
        return await this.connection.query(query, values);
    }

    async setup(params = {}) {
        const { host, port, database, username, password } = params;
        this.connection = new Database({
            url: `http://${host}:${port}`,
        })
        .useBasicAuth(username, password);

        if (database) {
            try {
                const info = await this.createDatabase(database, [
                    {
                        username,
                        passwd: password
                    }
                ]);
            } catch(e) {
                if (ArangoProvider.DUPLICATE_ITEM_CODE !== e.statusCode) {
                    console.error(e);
                    throw e;
                }
            }
    
            this.useDatabase(database);
        }
    }

    useDatabase(database) {
        try {
            this.connection.useDatabase(database);
        } catch(e) {
            throw e;
        }
    }

    async createCollection(collectionName, type = ArangoProvider.COLLECTION_TYPE_DOCUMENT) {
        if (this.database) {
            try {
                this.useDatabase(this.database);
            } catch(e) {
                throw e;
            }
        }

        let collection = this.resolveCollection(type, collectionName);
        this.registerCollectionType(collectionName, type);

        try {
            return await collection.create();
        } catch (e) {
            if (ArangoProvider.DUPLICATE_ITEM_CODE === e.statusCode) {
                return;
            }
            
            throw e;
        }
    }

    /**
     * 
     * @param {string} graphName 
     * @param {Object} properties 
     * 
     * Example:
     * 
     * {
        edgeDefinitions: [{
            collection: 'edges',
            from: ['start-vertices'],
            to: ['end-vertices']
        }
     */
    async createGraph(graphName, properties) {
        if (this.database) {
            try {
                this.useDatabase(this.database);
            } catch(e) {
                throw e;
            }
        }

        let graph = this.connection.graph(graphName);
        let info;

        try {
            info = await graph.create(properties);
        } catch(e) {
            if (ArangoProvider.DUPLICATE_ITEM_CODE === e.statusCode) {
                return;
            }
            
            throw e;
        }

        return info;
    }

    /**
     * 
     * @param {string} collectionName 
     * @param {string[]} fields 
     * @param {enum} type 
     * @param {Object} opts 
     * 
     * Note: will need to change to ensureIndex in ArangoJS v7
     */
    async createIndex(collectionName, fields = [], type = ArangoProvider.INDEX_TYPE_GENERAL, opts = {}) {
        if (this.database) {
            try {
                this.useDatabase(this.database);
            } catch(e) {
                throw e;
            }
        }

        const collection = this.connection.collection(collectionName);
        let index;

        try {
            index = await collection.createIndex({
                type,
                fields,
                opts,
            })
        } catch(e) {
            if (ArangoProvider.DUPLICATE_ITEM_CODE === e.statusCode) {
                return;
            }
            
            throw e;
        }

        return index;
    }

    async createDatabase(database) {
        let info;
        
        try {
            info = await this.connection.createDatabase(database);
        } catch(e) {
            if (ArangoProvider.DUPLICATE_ITEM_CODE === e.statusCode) {
                return;
            }
            
            throw e;
        }

        return info;
    }
}