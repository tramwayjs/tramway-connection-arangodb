Tramway ArangoProvider is a simple Provider add-on to simplify the process of implementing calls to arangodb databases and provides an async/await wrapper on the `arangojs` module.

# Installation:
1. `npm install tramway-connection-arangodb --save`

# Example project
https://gitlab.com/tramwayjs/tramway-connection-example

# Documentation

## Recommended Folder Structure in addition to Tramway
- config
- providers
- repositories
- entities

## ArangoProvider
The `ArangoProvider` is a derived `Provider` which follows the same interface.

### Configuration parameters
| Parameter | Default | Usage |
| --- | --- | --- |
| host | localhost | The host of your arangodb database |
| username | | The username for the database |
| password | | The password for the database |
| database |  | The name of the default database to connect to |
| port | | The port for the database |

## Getting started

Add the following to the imports and exports of the global parameters `/config/parameters/global/index.js`

```javascript
import { config as arangoConfig } from 'tramway-connection-arangodb';
export default {
    ...arangoConfig.parameters,
}
```

Add the following to the imports and exports of the services parameters `/config/services/index.js`

```javascript
import { config as arangoConfig } from 'tramway-connection-arangodb';

export default {
    ...arangoConfig.arangoServices,
};
```

Ensure the following environment variables are configured with the proper credentials:

```
ARANGO_PORT=8529
ARANGO_USERNAME=username
ARANGO_ROOT_PASSWORD=password
ARANGO_HOST=hostname
ARANGO_DATABASE=database
```

## Exposed Methods with this Library

### Provider

Note, the extended `ArangoProvider` expects an additional parameter `collectionName` on most methods. Using the `ArangoRepository` handles this for you. In addition, bulk inserts are possible.

| Function | Availability |
| ----- | ----- |
| ```async getOne(id: any, collectionName: string)``` | Available |
| ```async getMany(ids: any[], collectionName: string)``` | Available |
| ```async get(collectionName: string)``` | Available |
| ```async find(conditions: string/Object, collectionName: string)``` | Available |
| ```async has(id: any, collectionName: string)``` | Available |
| ```async hasThese(ids : any[], collectionName: string)``` | Available |
| ```async count(conditions: any, collectionName: string)``` | Available |
| ```async create(item: Entity/Object, collectionName: string)``` | Available |
| ```async createMany(item: Entity/Object[], collectionName: string)``` | Additional, creates a transaction for bulk inserts |
| ```async update(id: any, item: Entity/Object, collectionName: string)``` | Available |
| ```async replace(id: any, item: Entity/Object, collectionName: string)``` | Additional |
| ```async updateMany(item: Entity/Object[], collectionName: string)``` | Additional, creates a transaction for bulk updates |
| ```async delete(id: any, collectionName: string)``` | Available |
| ```async deleteMany(ids : any[], collectionName: string)``` | Available |
| ```async query(query: string/Object, values: Object)``` | Available |
| ```async setup(params: Object)``` | Available |
| ```async createCollection(collection: string, type: string)``` | Available |
| ```async createGraph(graphName: string, properties: Object)``` | Additional |
| ```async createIndex(collectionName: string, fields: string[], type: string, opts: Object)``` | Additional |
| ```useDatabase(database: string)``` | Additional |

#### Collection Types

The following collection types are supported when creating a new Collection:

| Name | Enum value |
| --- | --- |
| Document (Default) | `ArangoProvider.COLLECTION_TYPE_DOCUMENT` |
| Edge | `ArangoProvider.COLLECTION_TYPE_EDGE` |
| Graph Edge | `ArangoProvider.COLLECTION_GRAPH_TYPE_EDGE` |
| Graph Vertex | `ArangoProvider.COLLECTION_TYPE_GRAPH_VERTEX` |

Creating collections for graphs will require an additional step in their creation to avoid creating a new set of CRUD methods, or adding additional options to the existing and consistent interface. The provider contains its own resolvers to map collections to types and collections to graphs.

Creating a regular collection can be done in a repository `setup` method as follows:

```javascript
    try {
        info = await this.provider.createCollection(this.collection, ArangoProvider.COLLECTION_TYPE_EDGE);
    } catch(e) {
        throw e;
    }
```

Creating a graph collection will require an additional step to register the collection graph.

```javascript
    try {
        info = await this.provider.createCollection(this.collection, ArangoProvider.COLLECTION_TYPE_GRAPH_VERTEX);
        this.registerCollectionGraph(this.collection, graphName)
    } catch(e) {
        throw e;
    }
```

#### Index Types

The following index types are supported when creating a new Collection:

| Name | Enum value |
| --- | --- |
| General (Default) | `ArangoProvider.INDEX_TYPE_GENERAL` |
| Hash | `ArangoProvider.INDEX_TYPE_HASH` |
| Skip List | `ArangoProvider.INDEX_TYPE_SKIPLIST` |
| Geo | `ArangoProvider.INDEX_TYPE_GEO` |
| Full Text | `ArangoProvider.INDEX_TYPE_TEXT` |
| Persistent | `ArangoProvider.INDEX_TYPE_PERSISTENT` |
| TTL | `ArangoProvider.INDEX_TYPE_TTL` |

Creating an index in a repository `setup` method can be done as follows:

```javascript
    try {
        info = await this.provider.createIndex(this.collection, ['field1'], ArangoProvider.INDEX_TYPE_HASH, {unique: true, sparse: false});
    } catch(e) {
        throw e;
    }
```

#### Creating a Graph

Creating a graph in a repository `setup` method can be done as follows:

```javascript
    try {
        info = await this.provider.createGraph(graphName, {
            edgeDefinitions: [
                {
                    collection: 'edges',
                    from: ['start-vertices'],
                    to: ['end-vertices']
                },
            ]
        });
    } catch(e) {
        throw e;
    }
```

The graph properties uses the same interface as specified in the [ArangoDB JS Driver docs](https://www.arangodb.com/docs/stable/drivers/js-reference-graph.html#graphcreate).

### Repository

| Function | Usability |
| --- | --- |
| ```async exists(id: any)``` | Usable |
| ```async getOne(id: any)``` | Usable |
| ```async get()``` | Usable |
| ```async create(entity: Entity)``` | Usable |
| ```async createMany(entities: Entity[])``` | Additional |
| ```async update(entity: Entity)``` | Usable |
| ```async replace(entity: Entity)``` | Additional |
| ```async updateMany(entities: Entity[])``` | Additional |
| ```async delete(id: any)``` | Usable |
| ```async find(condtions: string/Object)``` | Usable |
| ```async getMany(ids: any[])``` | Usable |
| ```async count(conditions)``` | Usable |
| ```async setup()``` | Usable |