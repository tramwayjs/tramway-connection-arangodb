const assert = require('assert');
const utils = require('tramway-core-testsuite');
const lib = require('../dist/index.js');
var describeCoreClass = utils.describeCoreClass;
var describeFunction = utils.describeFunction;

describe("Simple acceptance tests to ensure library returns what's promised.", function(){
    describe("Should return a proper 'ArangoProvider' class", describeCoreClass(
        lib.default, 
        "ArangoProvider", 
        [],
        ["get", "getOne", "getMany", "has", "hasThese", "create", "createMany", "update", "replace", "updateMany", "delete", "deleteMany", "find", "count", "query", "setup", "createCollection", "createDatabase", "createIndex", "createGraph", "registerCollectionGraph", "registerCollectionType", "useDatabase", "resolveCollection", "getCollectionType", "getCollectionGraph"]
    ));

    describe("Should return an object for repositories.", function(){
        it("Should return an object for repositories.", function(){
            assert.strictEqual(typeof lib.repositories, "object");
        });
        it("There should be the same services as in the previous version", function(){
            assert.deepEqual(Object.keys(lib.repositories), ["ArangoRepository"]);
        });
        describe("Should return a proper 'ArangoRepository' class", describeCoreClass(
            lib.repositories.ArangoRepository, 
            "ArangoRepository", 
            [],
            ["createMany", "updateMany", "count"]    
        ));
    });
});